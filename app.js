const express = require('express');
const  path = require('path');
const config = require('dotenv').config()
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

var router = express.Router();

router.get('/', function (request, response) {
  response.render('index', { title: 'Welcome!' });
});

router.get('/student', function (request, response) {
  response.render('index', { title: 'Welcome, student!' });
});

router.get('/teacher', function (request, response) {
  response.render('index', { title: 'Welcome, teacher!' });
});

app.use('/', router);

app.listen(process.env.PORT, function () {
  console.log('Listening on port ' + process.env.PORT);
});
