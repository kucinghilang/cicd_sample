FROM node:14.15.1

COPY package*.json app.js ./
RUN npm config set package-lock false
RUN npm install
RUN npm audit fix
COPY . .

EXPOSE 8010
CMD [ "node", "server.js" ]
